require './person'

# :nodoc:
class Database
  def initialize
    @db = []
    load
  end

  def read
    @db
  end

  def add(p, persist)
    @db.push p
    save if persist
  end

  def search(val)
    a = []
    @db.each do |x|
      a.push x if (x.name == val) || (x.sur == val) || (x.age == val)
    end
    a
  end

  def delete(n)
    @db.delete_if { |x| x.id == n }
    save
  end

  def update(n, name, sur, age)
    x = @db.find { |y| y.id == n }
    x.name = name
    x.sur = sur
    x.age = age
    save
  end

  private

  def save
    f = File.open('baza.csv', 'w')
    @db.each do |x|
      f.puts "#{x.name},#{x.sur},#{x.age},#{x.id}"
    end
    f.close
  end

  def load
    return unless File.exist?('baza.csv')
    f = File.open('baza.csv', 'r')
    f.each do |x|
      dane = x.delete("\n").split(',')
      p = Person.new(dane[0], dane[1], dane[2], dane[3].to_i)
      add p, false
    end
    f.close
  end
end
