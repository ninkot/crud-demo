
require 'sinatra'
require './database'

configure :development do
  require 'better_errors'
  use BetterErrors::Middleware
  BetterErrors.application_root = __dir__
end

db = Database.new

get '/' do
  params[:page] = 1 if params[:page].to_i.zero?
  @users = if params[:search].nil? || params[:search].empty?
             db.read
           else
             db.search(params[:search])
           end
  @pages = (@users.length.to_f / 10.0).ceil
  @users = if params[:sort] == 'name'
             @users.sort { |x, y| x.name.downcase <=> y.name.downcase }
           elsif params[:sort] == 'sur'
             @users.sort { |x, y| x.sur.downcase <=> y.sur.downcase }
           else
             @users.sort { |x, y| x.age.to_i <=> y.age.to_i }
           end
  @users = @users.reverse if params[:order] == 'desc'
  @users = @users[(params[:page].to_i - 1) * 10, 10]

  haml :index
end

get '/add' do
  haml :add
end

post '/add' do
  p = Person.new(params[:name], params[:sur], params[:age])
  db.add p, true
  redirect to('/')
end

post '/delete' do
  db.delete(params[:id].to_i)
  redirect to('/')
end

get '/edit/:id' do |id|
  @user = db.read.find { |x| x.id == id.to_i }
  haml :edit
end

post '/update/:id' do |id|
  db.update(id.to_i, params[:name], params[:sur], params[:age])
  redirect to('/')
end

get '/custom.css' do
  content_type 'text/css'
  File.read('assets/custom.css')
end
