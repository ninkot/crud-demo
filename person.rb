# :nodoc:
class Person
  @@pk = 0
  attr_accessor :name, :sur, :age, :id
  def initialize(name, sur, age, id = nil)
    @name = name
    @sur = sur
    @age = age
    @@pk += 1
    @id = id ? id : @@pk
  end

  def name=(val)
    @name = !val.empty? ? val : @name
  end

  def sur=(val)
    @sur = !val.empty? ? val : @sur
  end

  def age=(val)
    @age = !val.empty? ? val : @age
  end
end
